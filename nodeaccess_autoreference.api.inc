<?php

/**
 * @file
 *   List of useful function related to references
 *
 *
 * @developers:
 *    Rafal Wieczorek <kenorb@gmail.com>
 */

/**
 * Get list of nids which are referenced to specified Node ID
 *
 * Example code:
 *  module_load_include('api.inc', 'nodeaccess_autoreference');
 *  $aok_nids = nodeaccess_autoreference_get_ref_nids('my_content', 'field_ref_foo', $nid);
 *
 * @param string $ctype
 *   content type 
 * @param string $user
 *   field name
 * @param integer $nid
 *   Node ID to search
 * @param integer $uid optional
 *   specify owner of the nodes
 * @return array matches
 *   returns all node IDs matches
 * 
 */
function nodeaccess_autoreference_get_ref_nids($ctype, $field_name, $nid, $uid = NULL) {
  module_load_include('inc', 'content', 'includes/content.crud');
  $field_cond = array('type_name' => $ctype, 'field_name' => $field_name);
  // $ct_table = _content_tablename('workbook_note', CONTENT_DB_STORAGE_PER_CONTENT_TYPE);
  if ($fields = content_field_instance_read($field_cond)) {
    $db_info = content_database_info($fields[0]); 
    $field['table_name'] = $db_info['table'];
    $field['value_type'] = key($db_info['columns']);
    $field['column_name'] = $db_info['columns'][$field['value_type']]['column'];
  }
  $res = db_result(db_query("
    SELECT GROUP_CONCAT(n.nid) FROM {node} n, {%s} c
    WHERE n.nid = c.nid
      AND n.type = '%s'
      AND %s = %d
      %s
    ", $field['table_name'], $ctype, $field['column_name'], $nid, ($uid ? " AND n.uid = $uid" : '')));
  return !empty($res) ? explode(',', $res) : array();
}

