<?php

/**
 * @file
 *   Include file
 *
 *
 * @developers:
 *    Rafal Wieczorek <kenorb@gmail.com>
 *    Konrad Wieczorek <kw.keth@gmail.com>
 */

/**
 * Find nid or uid connection by scanning the object
 *
 * @param object $obj
 *   node object
 * @param object $user
 *   current user object
 * @param array $path
 *   array of path passed by referenced (should be empty on first call)
 * @param integer $depth optional
 *   optional parameter for depth checking
 * @return matches
 *   returns all matches where connection has been found
 *
 */
function nodeaccess_autoreference_find_connection($node, $obj = NULL) {
  static $naar_counter_nocache = 0, $naar_counter_cache = 0;

  $res = NULL;
  $path = array();
  if (empty($obj)) {
    global $user;
    $obj = $user;
  }
  $obj_type = ($obj->nid ? 'nid' : 'uid');
  $obj_id = ($obj->nid ? $obj->nid : $obj->uid);
  $nid = is_object($node) ? $node->nid : $node;
  $cid = "nodeaccess_autoreference_$obj_type:$obj_id:nid:$nid";
  if (!variable_get('nodeaccess_autoreference_disable_cache', FALSE)
  && ($cache_data = cache_get($cid)->data)
  && $naar_counter_cache++ < variable_get('nodeaccess_autoreference_max_node_render_cache', 20)) {
    $cached = TRUE;
    $res = $path = $cache_data;
    $connected = (bool)!empty($cache_data);
  } else if ($naar_counter_nocache++ < variable_get('nodeaccess_autoreference_max_node_render_nocache', 10)) { // check the limit to not overload the page
    if (is_numeric($node)) {
      $node = node_load($node);
    }
    $connected = empty($connected) ? nodeaccess_autoreference_obj_connected($node, $obj, $path) : $connected;
    $connected = !$connected && variable_get('nodeaccess_autoreference_via_owner', TRUE) ? nodeaccess_autoreference_obj_connected(user_load($node->uid), $obj, $path) : $connected;
    cache_set($cid, $connected ? $path : NULL);
    $cached = FALSE;
    $res = $connected ? $path : NULL;
  } else {
    $res = FALSE; // return FALSE if limit has been exceeded
  }
  if ($res && variable_get('nodeaccess_autoreference_debug', FALSE)) {
    drupal_set_message(($cached ? t('Cached: ') : '')
    . t('Connected: %connected; Path between user %uid and node %nid (%type): %path',
    array('%nid' => $node->nid, '%uid' => $obj->uid, '%type' => $node->type, '%path' => !empty($path) ? print_r($path,true) : t('n/a'), '%connected' => ($path ? 'YES' : 'NO'))), 'status');
  }

  return $res;
}

/**
 * Find nid or uid connection by scanning the object
 *
 * @param object $obj
 *   node object
 * @param object $user
 *   current user object
 * @param array $path
 *   array of path passed by referenced (should be empty on first call)
 * @param integer $depth optional
 *   optional parameter for depth checking
 * @param text $private optional
 *   optional parameter for setting a node as "private" (ie no naar checking)
 * @return matches
 *   returns all matches where connection has been found
 *
 */
function nodeaccess_autoreference_obj_connected($obj, $user, &$path = array(), &$ignore_list = array(), $depth = 0, &$private = 'no') {
  if ($depth == 0) {
    $obj_id = ($obj->nid ? $obj->nid : $obj->uid);
    $path[] = "$obj->type|$type|" . $obj_id; // init path
    $path = array();
  } else if ($depth > variable_get('nodeaccess_autoreference_max_depth', 5)) {
    return FALSE;
  }
  $type = $obj->nid ? 'nid' : 'uid';
  if (nodeaccess_autoreference_same_objs($user, $obj)) { // if objects are the same, then mark as found
    $path[] = $user->uid.'_is_owner_of_' . $obj->nid;
    return TRUE;
  }

  $refs = nodeaccess_autoreference_obj_references($obj, variable_get('nodeaccess_autoreference_reverse_reference', FALSE), $private);
  foreach ($refs as $field_name => $ref) {
    global $na_skip_next_load;
    $na_skip_next_load = TRUE;
    $n_obj = NULL;
    if (isset($ref['nid'])) {
      $n_obj = node_load(array('nid' => $ref['nid']));
      $n_obj_id = $ref['nid'];
      $n_obj_type = 'nid';
    } else if (isset($ref['uid'])) {
      $n_obj = user_load(array('uid' => $ref['uid']));
      $n_obj_id = $ref['uid'];
      $n_obj_type = 'uid';
    }
    if (nodeaccess_autoreference_same_objs($user, $n_obj)) {
      $path[] = $field_name;

      return TRUE;
    } else if (!array_key_exists($ref['nid'], $ignore_list)) {
      if (variable_get('nodeaccess_autoreference_debug_more', FALSE)) {
        $path[] = $field_name; // considered paths - use for testing purposes
        drupal_set_message(print_r($path, true)); // for debug purposes
      }
      $ignore_list[$ref['nid']] = TRUE;
      $cid = "nodeaccess_autoreference_uid:$user->uid:$n_obj_type:$n_obj_id";
      if (variable_get('nodeaccess_autoreference_disable_cache', FALSE) && ($data = cache_get($cid)->data)) {
        foreach ($data as $key => $n_path) {
          $path[] = $n_path;
        }
        $ret = $data;
      } else {
        $ret = nodeaccess_autoreference_obj_connected($n_obj, $user, $path, $ignore_list, $depth+1, $private);
      }
    }
    if ($ret) {
      $path[] = $field_name;
      return $ret;
    } else {
    }
  }
  return FALSE;
}

/**
 * Compare the objects if are the same
 *
 * @param object $a
 *   could be user or node object
 * @param object $b
 *   could be user or node object
 * @return bool
 *   returns TRUE if objects are the same, otherwise FALSE
 *
 */
function nodeaccess_autoreference_same_objs($a, $b) {
  return ($a->uid == $b->uid && $a->type == $b->type);
}

/**
 * Find nid or uid connection by scanning the object
 *
 * @param object $obj
 *   could be user or node object
 * @return matches
 *   returns all matches where connection has been found
 *
 */
function nodeaccess_autoreference_obj_references($obj, $reverse = FALSE, &$private = 'no') { // data = user/node
  $matches = array();
  $type = $obj->nid ? 'nid' : 'uid';
  $id = $type == 'nid' ? $obj->nid : $obj->uid;

  if (is_array($obj) || is_object($obj)) {
    foreach ($obj as $field_name => $value) {
      if (is_array($value) || is_object($value)) {
        foreach ($value as $key2 => $value2) {
          if (is_array($value2)) {
            if (!empty($value2['value']) && $field_info = content_fields($field_name)) {
            //dsm(variable_get('naar_private_'.  $obj->type, 0));
              $privatefield = variable_get('naar_private_'.  $obj->type, 0);
              if ($field_info['type'] == 'text' && $privatefield[$obj->type][$field_name] == 1 && array_key_exists($field_name, $privatefield[$obj->type])) { /* support for private naar nodes */
                $private = 'yes';
              }
            }
            if (!empty($value2['uid']) && $field_info = content_fields($field_name)) {

              if ($field_info['type'] == 'userreference') { /* support for userreference module */
                $uid = $value2['uid'];
                $matches["$type:$id|$obj->type|$field_name|uid:$uid"]['uid'] = $uid;
              }
            } else if (!empty($value2['nid']) && $field_info = content_fields($field_name)) {
              if ($field_info['type'] == 'nodereference') { /* support for nodereference module */
                if(variable_get('naar_disable_'. $obj->type . '_'.$field_info['field_name'], FALSE) != TRUE) {
		  // if(in_array($field_info['field_name'],explode(' ', nodeaccess_autoreference_allowed())))
                    if ($private == 'yes') {
                      $allowed_fields = explode(' ', variable_get('nodeaccess_autoreference_private', ''));
                      if (in_array($field_name,$allowed_fields)) {
                        $nid = $value2['nid'];
                        $matches["$type:$id|$obj->type|$field_name|nid:$nid"]['nid'] = $nid;
                      }
                    } else {
                      $nid = $value2['nid'];
                      $matches["$type:$id|$obj->type|$field_name|nid:$nid"]['nid'] = $nid;	
                  } // end: else
                }
              }
            } // end: else if
          }
        } // end: foreach
      }
    } // end: foreach
    if (variable_get('nodeaccess_autoreference_via_owner', TRUE)) {
      $type == 'nid' ? $matches["uid:$obj->uid|via_owner|$type:$obj->nid:$obj->type"]['uid'] = $obj->uid : NULL;
    }
    if (variable_get('nodeaccess_autoreference_via_content_profile', TRUE) && module_exists('content_profile')) {
      $ptypes = content_profile_get_types();
      foreach ($ptypes as $key => $pnode) {
        if ($obj->type == $key) {
          $ptype = 1; 
        }
      }
      if ($type == 'nid' && isset($ptype) && $ptype == 1) {
        $matches["uid:$obj->uid|via_content_profile|$type:$obj->nid:$obj->type"]['uid'] = $obj->uid;
      } 
    }
    /* FIXME: multi-fields per one content are currently not supported */
    if ($reverse || variable_get('naar_reverse_'.  $obj->type . '_'.$field_info['field_name'], FALSE)) {
      /* find which content type can be referenced to current type */
      $ct_type = $obj->type; // current content type name
      $fields = content_fields(); // get info about all fields
      $referenced_type = array(); // init variable
      $referenced_roles = array(); // init variable
      foreach ($fields as $field_name => $field_data) {
        if ($field_data['type'] == 'nodereference' && $type == 'nid') { // consider only referenced fields
          /* get the node types which could be referenced to current node */
          $referenced_types = $field_data['referenceable_types'] ? drupal_map_assoc($field_data['referenceable_types']) : array(); // select types which can be referenced
          if (in_array($ct_type, $referenced_types)) {
            $referenced_type[$field_data['type_name']] = $ct_type; // set which types could be referenced to current type
            $field_names[$field_name] = $field_data['type'];
          }
        } else if ($field_data['type'] == 'userreference' && $type == 'uid') {
          /* get the roles which can be referenced to current node */
          $referenced_roles[$field_data['type_name']]  = ($field_data['referenceable_roles'] ? drupal_map_assoc(array_keys(array_flip($field_data['referenceable_roles']))) : array()); // select types which can be referenced
          $field_names_user[$field_name] = $field_data['type'];
        }
      }
      /* get all possible nids which could referencing to that type */
      if (!empty($referenced_type)) {
        $nid = $id; // current node id
        $ct_type = $obj->type; // current content type name
        $ref_types = implode("', '", array_keys($referenced_type));
        $result = db_query("SELECT nid FROM {node} WHERE type IN ('$ref_types') AND nid != $nid");
        while ($row = db_fetch_object($result)) {
          if ($node = node_load($row->nid)) {
            foreach ($field_names as $field_name_ok => $field_data) {
              if (property_exists($node, $field_name_ok)) {
                foreach ($node->{$field_name_ok} as $key => $id_obj) {
                  if ($id_obj['nid'] == $nid) {
                    $matches["nid:$nid:$type|rev_nid:$field_name_ok|nid:$node->nid"]['nid'] = $node->nid;
                  }
                }
              }
            }
          }
        } // end: while
      }

      if (!empty($referenced_roles)) {
        $uid = $id; // current user id
        $ref_types = implode(", ", array_keys($referenced_roles));
        $result = db_query("SELECT nid FROM {node} WHERE type IN ('$ref_types')");
        while ($row = db_fetch_object($result)) {
          if ($row = node_load($row->nid)) {
            foreach ($field_names_user as $field_name_ok => $field_data) {
              if (property_exists($row, $field_name_ok)) {
                foreach ($row->{$field_name_ok} as $key => $id_obj) {
                  if ($id_obj['uid'] == $id) {
                    $matches["uid:$id|rev_uid:$field_name_ok|nid:$row->nid:$row->type"]['nid'] = $row->nid;
                  }
                }
              }
            }
          }
        } // end: while
      }
    }
  }
  return $matches;
}

/**
 * Get array of grants for specified node
 *
 * @param object $node
 *   could be user or node object
 * @return array
 *   returns array of grants for current user, otherwise NULL
 *
 */
function nodeaccess_autoreference_node_get_grants($node, $user = NULL) {
  //module_load_include('inc', 'nodeaccess_autoreference'); // load additional function from included file
  if (empty($user)) {
    global $user;
  }
  if (is_numeric($node)) {
    $node = node_load($node);
  }
  $connected = nodeaccess_autoreference_find_connection($node, $user);
  if ($connected) {
    $grants = array();
    $grants[] = array(
      'nid' => $node->nid,
      'gid' => $user->uid,
      'realm' => 'nodeaccess_autoreference_owner',
      'grant_view' => '1',
      'grant_update' => user_access("edit referenced $node->type content"),
      'grant_delete' => user_access("delete referenced $node->type content"),
    );
    return $grants;
  }
  else {
    return NULL;
  }
}

/**
 * Update node grants
 *
 * @param object $node
 *   node object
 * @param object $grants
 *   grants object (can be retrieved from nodeaccess_autoreference_node_get_grants())
 *
 */
function nodeaccess_autoreference_node_write_grants($node, $grants) {
  node_access_write_grants($node, $grants, NULL, FALSE);
  if (variable_get('nodeaccess_autoreference_log', FALSE)) {
    $args = array('%nid' => $node->nid, '%permission' => print_r($grants, TRUE));
    $msg = 'Added permissions to node %nid. (permission: %permission)';
  }
}
/*
 * Function added for the "hack" in .module (in nodeapi) that looks for the referencing nodes
 * $obj is just the $node object.
 */
function nodeaccess_autoreference_reverse_check ($obj) {
$type = 'nid';
      /* find which content type can be referenced to current type */
      $ct_type = $obj->type; // current content type name
      $fields = content_fields(); // get info about all fields
      $referenced_type = array(); // init variable
      $referenced_roles = array(); // init variable
      foreach ($fields as $field_name => $field_data) {
        if ($field_data['type'] == 'nodereference' && $type == 'nid') { // consider only referenced fields
          /* get the node types which could be referenced to current node */
          $referenced_types = $field_data['referenceable_types'] ? drupal_map_assoc($field_data['referenceable_types']) : array(); // select types which can be referenced
          if (in_array($ct_type, $referenced_types)) {
            $referenced_type[$field_data['type_name']] = $ct_type; // set which types could be referenced to current type
            $field_names[$field_name] = $field_data['type'];
          }
        } 
      }
      /* get all possible nids which could referencing to that type */
      if (!empty($referenced_type)) {
        $nid = $obj->nid; // current node id
        $ct_type = $obj->type; // current content type name
        $ref_types = implode("', '", array_keys($referenced_type));
        $result = db_query("SELECT nid FROM {node} WHERE type IN ('$ref_types') AND nid != $nid");
        while ($row = db_fetch_object($result)) {
          if ($node = node_load($row->nid)) {
            foreach ($field_names as $field_name_ok => $field_data) {
              if (property_exists($node, $field_name_ok)) {
                foreach ($node->{$field_name_ok} as $key => $id_obj) {
                  if ($id_obj['nid'] == $nid) {
                    $matches2["nid:$nid:$type|rev_nid:$field_name_ok|nid:$node->nid"]['nid'] = $node->nid;
                  }
                }
              }
            }
          }
        } // end: while
      }

      if (!empty($referenced_roles)) {
        $uid = $id; // current user id
        $ref_types = implode(", ", array_keys($referenced_roles));
        $result = db_query("SELECT nid FROM {node} WHERE type IN ('$ref_types')");
        while ($row = db_fetch_object($result)) {
          if ($row = node_load($row->nid)) {
            foreach ($field_names_user as $field_name_ok => $field_data) {
              if (property_exists($row, $field_name_ok)) {
                foreach ($row->{$field_name_ok} as $key => $id_obj) {
                  if ($id_obj['uid'] == $id) {
                    $matches2["uid:$id|rev_uid:$field_name_ok|nid:$row->nid:$row->type"]['nid'] = $row->nid;
                  }
                }
              }
            }
          }
        } // end: while
      }
    return $matches2;

}
