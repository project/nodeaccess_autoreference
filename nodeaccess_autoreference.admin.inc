<?php

/**
 * @file
 *   Form settings include file
 *
 * @version
 *
 * @developers
 *   Rafal Wieczorek <kenorb@gmail.com>
 *   Konrad Wieczorek <kw.keth@gmail.com>
 */

/**
 * Menu callback for the settings form.
 */
function nodeaccess_autoreference_admin_form() {

  $form['nodeaccess_autoreference'] = array(
    '#type' => 'fieldset',
    '#title' => t('Form Settings'),
    '#description' => t('Addition permission for edit and delete operations you can find in Drupal Permissions table.'),
    '#collapsible' => TRUE,
  ); 

  $form['nodeaccess_autoreference']['nodeaccess_autoreference_views_check'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable reference checking on Views.'),
    '#description' => t('It will check possible referenced permissions on Views loading.'),
    '#default_value' => variable_get('nodeaccess_autoreference_views_check', FALSE),
    '#disabled' => !module_exists('views'),
  );

  $form['nodeaccess_autoreference']['nodeaccess_autoreference_via_owner'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check permission via owners.'),
    '#description' => t('Select if you want to enable checking permissions through owners. Especially when you have references to user node profiles.'),
    '#default_value' => variable_get('nodeaccess_autoreference_via_owner', TRUE),
  );
  //Allows you to check per content profile.  This is usually done with "via owner" but sometimes you just want to check per content profile and not via owner on all nodes.
  $form['nodeaccess_autoreference']['nodeaccess_autoreference_via_content_profile'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check permission via content profile.'),
    '#description' => t('Select if you want to enable checking permissions through content profile.'),
    '#default_value' => variable_get('nodeaccess_autoreference_via_content_profile', TRUE),
  );

  $form['nodeaccess_autoreference']['nodeaccess_autoreference_rebuild'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild permissions'),
    '#submit' => array('nodeaccess_autoreference_rebuild_submit'),
  );
  //Allows you to enter the fields that NAAR will go threw even if the node is set to "private"
  $form['nodeaccess_autoreference_private'] = array(
    '#type' => 'fieldset',
    '#title' => t('"Private" Settings'),
    '#description' => t('Enter in the fields you want to allow NAAR to pass through on "private" nodes.  This only works for nodereference fields.'),
    '#collapsible' => TRUE,
  );
  $form['nodeaccess_autoreference_private']['nodeaccess_autoreference_private'] = array(
    '#type' => 'textfield',
    '#title' => t('Private Fields'),
    '#description' => t('Even if nodes are marked as private, NAAR will still pass through and check access through these fields. <br/> enter fields separated by a space.  Example:field_project field_clients'),
    '#default_value' => variable_get('nodeaccess_autoreference_private', t('')),
  );

  $form['nodeaccess_autoreference_adv'] = array(
    '#type' => 'fieldset',
    '#title' => t('Advanced Settings'),
    '#description' => t('Change this settings if you really know what you are doing. You could loss your website performance or even have some crashes.'),
    '#collapsible' => TRUE,
  );


  $form['nodeaccess_autoreference_adv']['nodeaccess_autoreference_reverse_reference'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable reverse references checking.'),
    '#description' => t('Give permission to parent nodes which have no references it-self, but other nodes referencing to it.'),
    '#default_value' => variable_get('nodeaccess_autoreference_reverse_reference', FALSE),
  );

  $form['nodeaccess_autoreference_adv']['nodeaccess_autoreference_form_alter_check'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable reference checking on forms.'),
    '#description' => t('It will check possible referenced permissions on forms loading. Do not use this option if your forms are loading very slowly.'),
    '#default_value' => variable_get('nodeaccess_autoreference_form_alter_check', FALSE),
  );

  $form['nodeaccess_autoreference_adv']['nodeaccess_autoreference_max_depth'] = array(
    '#type' => 'textfield',
    '#title' => t('Max depth connection limit to check'),
    '#size' => 5,
    '#description' => t('Default max depth connection limit.'),
    '#default_value' => variable_get('nodeaccess_autoreference_max_depth', 5),
  );

  $form['nodeaccess_autoreference_adv']['nodeaccess_autoreference_max_node_render_nocache'] = array(
    '#type' => 'textfield',
    '#title' => t('Max non-cached connection to check at once'),
    '#size' => 5,
    '#description' => t('How many nodes can be checked for referenced path per one page refresh.'),
    '#default_value' => variable_get('nodeaccess_autoreference_max_node_render_nocache', 10),
  );

  $form['nodeaccess_autoreference_adv']['nodeaccess_autoreference_max_node_render_cache'] = array(
    '#type' => 'textfield',
    '#title' => t('Max cached connection to check at once'),
    '#size' => 5,
    '#description' => t('How many nodes can be checked for referenced path per one page refresh.'),
    '#default_value' => variable_get('nodeaccess_autoreference_max_node_render_cache', 20),
  );

  $form['nodeaccess_autoreference_log'] = array(
    '#type' => 'fieldset',
    '#title' => t('Debug stuff'),
    '#description' => t('Use those settings only for testing purposes!.'),
    '#collapsible' => TRUE,
  ); 

  $form['nodeaccess_autoreference_log']['nodeaccess_autoreference_log'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log all permission changes.'),
    '#description' => t('Select if you want to log all permission changes.'),
    '#default_value' => variable_get('nodeaccess_autoreference_log', FALSE),
  );
  $form['nodeaccess_autoreference_log']['nodeaccess_autoreference_debug'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show connection messages.'),
    '#description' => t('Select if you want to show messages with connections.'),
    '#default_value' => variable_get('nodeaccess_autoreference_debug', FALSE),
  );
  $form['nodeaccess_autoreference_log']['nodeaccess_autoreference_debug_more'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show more connection messages.'),
    '#description' => t('Select if you want to show additional messages paths. Use it only for debug purposes.'),
    '#default_value' => variable_get('nodeaccess_autoreference_debug_more', FALSE),
  );
  $form['nodeaccess_autoreference_log']['nodeaccess_autoreference_disable_cache'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable connection caching.'),
    '#description' => t('Select if you want temporary to disable caching the permissions. It could slow down referenced permission checking.'),
    '#default_value' => variable_get('nodeaccess_autoreference_disable_cache', FALSE),
  );
  $form['nodeaccess_autoreference_log']['nodeaccess_autoreference_disable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable updating the permissions.'),
    '#description' => t('Select if you want temporary to disable giving the permissions if referenced path has been found.'),
    '#default_value' => variable_get('nodeaccess_autoreference_disable', FALSE),
  );
  //$form['#validate'] = array('nodeaccess_autoreference_admin_form_validate');

  return system_settings_form($form); 
}
 
/**
 * Form API callback to validate the settings form.
 */
function nodeaccess_autoreference_admin_form_validate($form, &$form_state) {
  $values = &$form_state['values'];
  if (!is_numeric($values['nodeaccess_autoreference_max_depth'])) {
    form_set_error('nodeaccess_autoreference_max_depth', t('Depth limit must be a number greater than 0.')); 
  }
} 

/**
 * Form button submit callback.
 */
function nodeaccess_autoreference_rebuild_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/settings/nodeaccess_autoreference/rebuild';
}
 
/**
 * Menu callback: confirm rebuilding of permissions.
 */
function nodeaccess_autoreference_rebuild_confirm() {
  return confirm_form(array(), t('Are you sure you want to rebuild the permissions on site content?'),
          'admin/settings/nodeaccess_autoreference', t('This action rebuilds all permissions on site content, and may be a lengthy process. This action cannot be undone.'), t('Rebuild referenced permissions'), t('Cancel'));
} 

/**
 * Handler for wipe confirmation
 */
function nodeaccess_autoreference_rebuild_confirm_submit($form, &$form_state) {
  node_access_rebuild(TRUE);
  drupal_set_message(t('Referenced permission have been rebuilt.')); 
  $form_state['redirect'] = 'admin/settings/nodeaccess_autoreference';
}
 
/**
 * Form button submit callback.
 */
function wsod_admin_form_submit($form, &$form_state) {
  $form_state['redirect'] = 'admin/settings/nodeaccess_autoreference';
} 

